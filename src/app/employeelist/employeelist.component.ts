import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css']
})
export class EmployeelistComponent implements OnInit {
  data: any=[];
  id: any;

  constructor(private service:EmployeeService) { }

  getEmployee(){
    this.service.get().subscribe(res=> this.data=res);
  }
  setIdDelete(id: any){
    this.id=id;
    console.log(id+" this is :"+this.id);
    this.service.setIdDelete(this.id);
  }
  setIdUpdate(id: any){
    this.id=id;
    console.log(id+" this is :"+this.id);
    this.service.setIdUpdate(this.id);
  }

  ngOnInit() {
    this.getEmployee();
  }

}
