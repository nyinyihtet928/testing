import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Http, Response, URLSearchParams} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  idDel: any;
  idUpdate: any;
  
  get(){
    return this.http.get("http://dummy.restapiexample.com/api/v1/employees");
  }
  add(employee) {
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', employee);
    }
  getid(id: any){
    return this.http.get("http://dummy.restapiexample.com/api/v1/employee/"+id);
  }
  update(id: any, employee){
    return this.http.put("http://dummy.restapiexample.com/api/v1/update/"+id, employee);
  }
  delete(id: any){
    return this.http.delete("http://dummy.restapiexample.com/api/v1/delete/"+id);
  }
  setIdDelete(id: any){
    this.idDel=id;
  }
  getIdDelete(){
    return this.idDel;
  }
  setIdUpdate(id: any){
    this.idUpdate=id;
    console.log(this.idUpdate);
  }
  getIdUpdate(){
    return this.idUpdate;
  }

}
