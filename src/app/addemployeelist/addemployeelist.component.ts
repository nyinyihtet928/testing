import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-addemployeelist',
  templateUrl: './addemployeelist.component.html',
  styleUrls: ['./addemployeelist.component.css']
})
export class AddemployeelistComponent implements OnInit {

  constructor(private router:Router,private service:EmployeeService) { }
  employee ={"name":"","salary":"","age":""};

  addEmployee(){
    this.service.add(this.employee).subscribe(res=> {console.log(res)});
    //this.data=this.service.add(this.Name, this.Salary,this.Age);
  }

  ngOnInit() {
  }

}
